# Quizzler
A simple quiz app built with Swift 3 running on iOS 10.
This is app uses content provided by London App Brewery.

## Installation
+ Clone the repository
+ Open the project in XCode 8+
+ Run the app to deploy to the emulator or phone

## Author
John Kennedy [@codejockie](https://twitter.com/codejockie)
